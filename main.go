package main

import (
	"fmt"
	"math"
	"udevsLesson/algoritms/counting"
	"udevsLesson/algoritms/mergeSort"
	"udevsLesson/algoritms/binarySearch"
)

func main() {
	//counting sort
	arr := []int{1,2,3,3,3,4,5}
	max := math.MinInt
	for i := 0; i < len(arr); i++ {
		if arr[i] > max {
			max = arr[i]
		}
	}
	result := counting.CountingSort(arr, max)
	fmt.Println(result)

	//merge sort

	resultMergeSort := mergesort.MergeSort(arr)
	fmt.Println(resultMergeSort)

	//binarySearch

	upperBound:=binarysearch.UpperBound(arr,3)
	fmt.Println(upperBound)

	lowerBound:=binarysearch.LowerBound(arr,3)
	fmt.Println(lowerBound)

}
